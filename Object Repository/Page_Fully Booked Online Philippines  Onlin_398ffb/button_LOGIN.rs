<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_LOGIN</name>
   <tag></tag>
   <elementGuidId>37926191-0030-4367-b649-36569c46fafa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/section/div/div/form/div[4]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.Button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>914313fd-202b-4f53-a97d-d0dbe3ae7383</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>Button</value>
      <webElementGuid>9621615f-2fdc-4619-8bd9-81d1c42eb7f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LOGIN</value>
      <webElementGuid>bf0b862d-b3c0-47cc-9e4d-70ed2e69f941</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;LocalizationWrapper&quot;]/div[@class=&quot;Router-MainItems&quot;]/section[@class=&quot;LoginAccount&quot;]/div[@class=&quot;ContentWrapper&quot;]/div[@class=&quot;LoginAccount-InnerWrapper&quot;]/form[@class=&quot;Form&quot;]/div[@class=&quot;MyAccountOverlay-Additional&quot;]/button[@class=&quot;Button&quot;]</value>
      <webElementGuid>4f50785c-14d2-49da-9760-74242b326b74</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/section/div/div/form/div[4]/button</value>
      <webElementGuid>a2f82bdf-831c-4720-a38b-265c275e4765</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button</value>
      <webElementGuid>79f8e263-ae52-4617-a03e-c5d431d2ae0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'LOGIN' or . = 'LOGIN')]</value>
      <webElementGuid>0fbda7ef-d48a-4c6b-9e7e-c1f1e9f29660</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
